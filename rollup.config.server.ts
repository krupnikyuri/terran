// todo fix
// Error: Cannot find module './users'

import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
// import external from 'rollup-plugin-peer-deps-external';
import postcss from 'rollup-plugin-postcss';
import autoprefixer from 'autoprefixer';
import typescript from "@rollup/plugin-typescript";
import run from '@rollup/plugin-run';

const globals = {
    react: 'React',
    'prop-types': 'PropTypes'
};

const defaultModule = {
    input: 'src/index.ts',
    output: [
        {
            dir: 'dist',
            format: 'cjs',
            // globals
        },
        // {
        //     dir: 'dist/esm',
        //     format: 'esm',
        //     globals
        // }
    ],
    plugins: [
        babel({}),
        typescript({}),
        // external({
        //     includeDependencies: true
        // }),
        postcss({
            minimize: true,
            modules: true,
            plugins: [autoprefixer()]
        }),
        resolve({
            extensions: ['.tsx', '.ts', '.mjs', '.js', '.jsx', '.json', '.css', '.scss', '.less']
        }),
        // commonjs({}),
        run(),
    ]
};

export default defaultModule;
