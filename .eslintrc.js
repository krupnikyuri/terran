module.exports = {
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint", "react"],
    rules: {
        // "@typescript-eslint/rule-name": "error",
        // "indent": ["off", 2, {
        //     "ignoredNodes": ["TemplateLiteral"]
        // }],
        "comma-dangle": 0,

        "no-underscore-dangle": 1,
        // 'react/jsx-indent': [2, 4], // personal
        // 'react/js/x-indent-props': 0, // personal
        // 'jsx-a11y/anchor-is-valid': ['error', {
        //     'components': ['Link'],
        //     'specialLink': ['to'],
        //     'aspects': ['noHref', 'invalidHref', 'preferButton']
        // }],
        // TODO: Remove when is https://github.com/babel/babel-eslint/issues/530 fixed
        "template-curly-spacing": "off",
    },
    // https://github.com/airbnb/javascript/blob/master/packages/eslint-config-airbnb/rules/react.js
    // extends: ['airbnb'],
    // plugins: ['jsdoc'],
    extends: ["plugin:@typescript-eslint/recommended", "plugin:react/recommended"],
    // env: {
    //     jest: true
    // }
    overrides: [
        {
            files: ["*.tsx"],
            rules: {
                "react/prop-types": "off",
            },
        },
    ],
};
